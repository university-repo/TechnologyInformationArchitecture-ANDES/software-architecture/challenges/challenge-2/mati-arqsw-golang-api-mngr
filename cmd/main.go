package main

import (
    "mati-arqsw-golang-api-mngr/internal/adapters/handlers"
    "mati-arqsw-golang-api-mngr/internal/adapters/middlewares"
    usecases "mati-arqsw-golang-api-mngr/internal/application/useCases"
    "mati-arqsw-golang-api-mngr/internal/infrastructure/config"
    "mati-arqsw-golang-api-mngr/internal/infrastructure/logger"
    "mati-arqsw-golang-api-mngr/internal/infrastructure/server"
    "github.com/gin-gonic/gin"
    "github.com/joho/godotenv"
    "os"
    "os/signal"
    "syscall"
)

func main() {
    logger.InitLogger()
    defer logger.StopLogger()

    if err := godotenv.Load(); err != nil {
        logger.Logger.Error("Error loading .env file")
    }

    instance := gin.New()
    instance.Use(gin.Recovery())

    headersMiddleware := middlewares.NewHeadersMiddleware()
    instance.Use(headersMiddleware.RequireHeaders)

    taxService := usecases.NewTaxService()
    taxHandler := handlers.NewTaxHandler(instance, taxService)
    taxHandler.InitTransactionRoutes()

    httpServer := server.NewHttpServer(
        instance,
        config.HttpServerConfig{
            Port: 9081,
        },
    )

    httpServer.Start()
    defer httpServer.Stop()

    c := make(chan os.Signal, 1)
    signal.Notify(c, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
    <-c
    logger.Logger.Info("Server shut down")
}
