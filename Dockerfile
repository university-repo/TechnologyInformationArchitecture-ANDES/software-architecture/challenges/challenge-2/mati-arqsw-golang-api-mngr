FROM golang:1.21.1

RUN mkdir /app

WORKDIR /app

COPY go.mod /app/
COPY go.sum /app/
COPY go.work.sum /app/

RUN go mod download

COPY . /app

RUN CGO_ENABLED=0 GOOS=linux go build -o /mati-arqsw-golang-api-mngr ./cmd

EXPOSE 8080

CMD ["/mati-arqsw-golang-api-mngr"]
