package middlewares

import (
	"net/http"
	"mati-arqsw-golang-api-mngr/internal/infrastructure/logger"
	"github.com/gin-gonic/gin"
)

type HeadersMiddleware struct {
	XRqUID string `header:"X-RqUID" binding:"required"`
	XName  string `header:"X-Name" binding:"required"`
}

func NewHeadersMiddleware() *HeadersMiddleware {
	return &HeadersMiddleware{}
}

func (h *HeadersMiddleware) RequireHeaders(c *gin.Context) {
	var headers HeadersMiddleware
	if err := c.ShouldBindHeader(&headers); err != nil {
		logger.Logger.Error("[HEADERS MIDDLEWARE] Missing required headers in request")
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": "Missing required headers",
		})
		return
	}
	c.Next()
}
