package handlers

import (
    "net/http"
    "github.com/gin-gonic/gin"
    usecases "mati-arqsw-golang-api-mngr/internal/application/useCases"
    "mati-arqsw-golang-api-mngr/internal/infrastructure/logger"
    "go.uber.org/zap"
)

type TaxHandler struct {
    gin           *gin.Engine
    taxService *usecases.TaxService
}

type TaxRequest struct {
    ID       string `json:"id"`
    Amount   int    `json:"amount"`
    IDClient string `json:"idClient"`
    Email    string `json:"email"`
}

func NewTaxHandler(gin *gin.Engine, service *usecases.TaxService) *TaxHandler { // Constructor corrected
    return &TaxHandler{
        gin:           gin,
        taxService: service,
    }
}

func (h *TaxHandler) InitTransactionRoutes() {
    transactionRoute := h.gin.Group("/v1/product/tax")
    transactionRoute.POST("/", h.HandleTransaction)
}

func (h *TaxHandler) HandleTransaction(c *gin.Context) {
    var request TaxRequest
    if err := c.ShouldBindJSON(&request); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    tax := h.taxService.CalculateTax(request.Amount)

    logger.Logger.Info("Calculated tax", zap.String("tax", tax.String()))

    c.JSON(http.StatusOK, gin.H{"IVA": tax.String()}) // Send tax as a string to ensure precision
}