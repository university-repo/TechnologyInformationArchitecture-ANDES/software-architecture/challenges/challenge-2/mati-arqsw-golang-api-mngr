package constants

const (
	AuthenticationSuccess string = "authentication_success"
	AuthenticationFailed  string = "authentication_failed"
	EnrollmentSuccess     string = "enrollment_success"
	EnrollmentFailed      string = "enrollment_failed"
)
