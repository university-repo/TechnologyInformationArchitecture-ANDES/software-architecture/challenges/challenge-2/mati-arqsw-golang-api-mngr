package logger

import (
	"go.uber.org/zap"
)

var Logger *zap.Logger

func InitLogger() {
	Logger, _ = zap.NewDevelopment()
}

func StopLogger() {
	_ = Logger.Sync()
}
