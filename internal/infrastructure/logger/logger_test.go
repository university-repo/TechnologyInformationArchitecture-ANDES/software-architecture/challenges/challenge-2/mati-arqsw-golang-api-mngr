package logger

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitLogger(t *testing.T) {
	t.Run("Init Logger", func(t *testing.T) {
		InitLogger()
		assert.NotNil(t, Logger)
	})

	t.Run("Stop Logger", func(t *testing.T) {
		StopLogger()
		assert.NotNil(t, Logger)
	})
}
