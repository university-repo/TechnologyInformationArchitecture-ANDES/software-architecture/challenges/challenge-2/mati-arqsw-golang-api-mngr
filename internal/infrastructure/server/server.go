package server

import (
	"context"
	"mati-arqsw-golang-api-mngr/internal/infrastructure/config"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

const defaulHost = "localhost"

type HttpServer interface {
	Start()
	Stop()
}

type httpServer struct {
	Port   int
	server *http.Server
}

var _ HttpServer = (*httpServer)(nil)

func NewHttpServer(
	router *gin.Engine,
	config config.HttpServerConfig,
) HttpServer {
	return &httpServer{
		Port: config.Port,
		server: &http.Server{
			Addr:    fmt.Sprintf("%s:%d", defaulHost, config.Port),
			Handler: router,
		},
	}
}

func (h *httpServer) Start() {
	go func() {
		if err := h.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf(
				"Failed to start HttpServer in port %d. Error: %s",
				h.Port,
				err,
			)
		}
	}()
	log.Printf("Server running in port %d", h.Port)
}

func (h *httpServer) Stop() {
	ctx, cancel := context.WithTimeout(
		context.Background(), time.Duration(3)*time.Second,
	)
	defer cancel()

	if err := h.server.Shutdown(ctx); err != nil {
		log.Fatalf("Server forced to shutdown. Error: %s", err)
	}
}
