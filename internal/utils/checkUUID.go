package utils

import "github.com/google/uuid"

func CheckUUID(reqUuid string) string {
	if err := uuid.Validate(reqUuid); err != nil {
		return uuid.NewString()
	}
	return reqUuid
}
