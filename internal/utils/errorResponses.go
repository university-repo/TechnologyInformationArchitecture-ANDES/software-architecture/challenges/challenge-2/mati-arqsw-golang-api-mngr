package utils

import (
	domain "mati-arqsw-golang-api-mngr/internal/domain/entities"
	"net/http"
)

var BadRequestResponse = &domain.Response{
	ErrorCode: http.StatusBadRequest,
	Message:   "Bad request",
	Data:      nil,
}

var UnauthorizedResponse = domain.Response{
	ErrorCode: http.StatusUnauthorized,
	Message:   "Unauthorized",
	Data:      nil,
}

var InternalServerResponse = domain.Response{
	ErrorCode: http.StatusInternalServerError,
	Message:   "Internal server error",
	Data:      nil,
}
