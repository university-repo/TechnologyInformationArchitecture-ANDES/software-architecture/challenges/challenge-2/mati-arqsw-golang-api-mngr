package utils

import (
	"testing";
	"github.com/stretchr/testify/assert";
)

var mockUUID = "00000000-0000-0000-0000-000000000000"

func TestCheckUUID(t *testing.T) {

	t.Run("Valid UUID", func(t *testing.T) {
		result := CheckUUID(mockUUID)
		assert.Equal(t, mockUUID, result)
	})

	t.Run("Invalid UUID", func(t *testing.T) {
		result := CheckUUID("x")
		assert.NotEqual(t, "x", result)
	})
}
