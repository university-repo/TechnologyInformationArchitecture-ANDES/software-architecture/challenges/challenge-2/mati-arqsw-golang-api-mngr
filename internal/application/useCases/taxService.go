package usecases

import (
	"github.com/shopspring/decimal" // Import the decimal package
)

type TaxService struct{}

func NewTaxService() *TaxService {
	return &TaxService{}
}

func (u *TaxService) CalculateTax(amount int) decimal.Decimal {
	iva := decimal.NewFromFloat(0.19)
	amountDecimal := decimal.NewFromInt(int64(amount))
	calculatedTax := amountDecimal.Mul(iva)
	return calculatedTax
}